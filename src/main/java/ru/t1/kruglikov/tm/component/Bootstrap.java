package ru.t1.kruglikov.tm.component;

import ru.t1.kruglikov.tm.api.controller.*;
import ru.t1.kruglikov.tm.api.repository.ICommandRepository;
import ru.t1.kruglikov.tm.api.repository.IProjectRepository;
import ru.t1.kruglikov.tm.api.repository.ITaskRepository;
import ru.t1.kruglikov.tm.api.service.*;
import ru.t1.kruglikov.tm.constant.ArgumentConst;
import ru.t1.kruglikov.tm.constant.CommandConst;
import ru.t1.kruglikov.tm.controller.*;
import ru.t1.kruglikov.tm.enumerated.Status;
import ru.t1.kruglikov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.kruglikov.tm.exception.system.CommandNotSupportedException;
import ru.t1.kruglikov.tm.model.Project;
import ru.t1.kruglikov.tm.model.Task;
import ru.t1.kruglikov.tm.repository.*;
import ru.t1.kruglikov.tm.service.*;
import ru.t1.kruglikov.tm.util.TerminalUtil;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService, commandRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final IProjectController projectController = new ProjectController(projectService, projectTaskService);

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectTaskController projectTaskController = new ProjectTaskController(projectTaskService);

    private final ILoggerService loggerService = new LoggerService();

    private void processArguments(final String[] args) {
        try {
            if (args == null || args.length == 0) return;
            processArgument(args[0]);
            System.out.println("[OK]");
            System.exit(0);
        } catch (final RuntimeException e) {
            loggerService.error(e);
            System.out.println("[ERROR]");
            System.exit(1);
        }
    }

    private void processArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConst.ABOUT:
                commandController.displayAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.displayVersion();
                break;
            case ArgumentConst.HELP:
                commandController.displayHelp();
                break;
            case ArgumentConst.INFO:
                commandController.displayInfo();
                break;
            case ArgumentConst.ARGUMENT:
                commandController.displayArguments();
                break;
            case ArgumentConst.COMMAND:
                commandController.displayCommands();
                break;
            default:
                throw new ArgumentNotSupportedException(arg);
        }
    }

    private void processCommand(final String command) {
        if (command == null || command.isEmpty()) throw new CommandNotSupportedException();
        switch (command) {
            case CommandConst.ABOUT:
                commandController.displayAbout();
                break;
            case CommandConst.VERSION:
                commandController.displayVersion();
                break;
            case CommandConst.HELP:
                commandController.displayHelp();
                break;
            case CommandConst.INFO:
                commandController.displayInfo();
                break;
            case CommandConst.EXIT:
                exit();
                break;
            case CommandConst.ARGUMENT:
                commandController.displayArguments();
                break;
            case CommandConst.COMMAND:
                commandController.displayCommands();
                break;
            case CommandConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case CommandConst.PROJECT_LIST:
                projectController.showProjects();
                break;
            case CommandConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case CommandConst.TASK_CREATE:
                taskController.createTask();
                break;
            case CommandConst.TASK_LIST:
                taskController.showTask();
                break;
            case CommandConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case CommandConst.PROJECT_REMOVE_BY_ID:
                projectController.removeProjectById();
                break;
            case CommandConst.PROJECT_REMOVE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case CommandConst.PROJECT_SHOW_BY_ID:
                projectController.showProjectById();
                break;
            case CommandConst.PROJECT_SHOW_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case CommandConst.PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            case CommandConst.PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case CommandConst.TASK_REMOVE_BY_ID:
                taskController.removeTaskById();
                break;
            case CommandConst.TASK_REMOVE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case CommandConst.TASK_SHOW_BY_ID:
                taskController.showTaskById();
                break;
            case CommandConst.TASK_SHOW_BY_INDEX:
                taskController.showTaskByIndex();
                break;
            case CommandConst.TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case CommandConst.TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case CommandConst.PROJECT_CHANGE_STATUS_BY_ID:
                projectController.changeProjectStatusById();
                break;
            case CommandConst.PROJECT_CHANGE_STATUS_BY_INDEX:
                projectController.changeProjectStatusByIndex();
                break;
            case CommandConst.PROJECT_START_BY_ID:
                projectController.startProjectById();
                break;
            case CommandConst.PROJECT_START_BY_INDEX:
                projectController.startProjectByIndex();
                break;
            case CommandConst.PROJECT_COMPLETE_BY_ID:
                projectController.completeProjectById();
                break;
            case CommandConst.PROJECT_COMPLETE_BY_INDEX:
                projectController.completeProjectByIndex();
                break;
            case CommandConst.TASK_CHANGE_STATUS_BY_ID:
                taskController.changeTaskStatusById();
                break;
            case CommandConst.TASK_CHANGE_STATUS_BY_INDEX:
                taskController.changeTaskStatusByIndex();
                break;
            case CommandConst.TASK_START_BY_ID:
                taskController.startTaskById();
                break;
            case CommandConst.TASK_START_BY_INDEX:
                taskController.startTaskByIndex();
                break;
            case CommandConst.TASK_COMPLETE_BY_ID:
                taskController.completeTaskById();
                break;
            case CommandConst.TASK_COMPLETE_BY_INDEX:
                taskController.completeTaskByIndex();
                break;
            case CommandConst.TASK_BIND_TO_PROJECT:
                projectTaskController.bindTaskToProject();
                break;
            case CommandConst.TASK_UNBIND_FROM_PROJECT:
                projectTaskController.unbindTaskFromProject();
                break;
            case CommandConst.TASK_SHOW_BY_PROJECT_ID:
                taskController.showTasksByProjectId();
                break;
            default:
                throw new CommandNotSupportedException(command);
        }
    }

    private void exit() {
        System.exit(0);
    }

    public void start(final String[] args) {
        initLogger();
        processArguments(args);
        initDemoData();
        try {
            System.out.println("ENTER COMMAND:");
            final String command = TerminalUtil.nextLine();
            processCommand(command);
            System.out.println("[OK]");
            loggerService.command(command);
        } catch (final RuntimeException e) {
            loggerService.error(e);
            System.out.println("[ERROR]");
        }
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
            }
        });
    }

    private void initDemoData() {
        projectService.add(new Project("PROJECT_01", "Test project 1", Status.COMPLETED));
        projectService.add(new Project("PROJECT_40", "Test project 40", Status.IN_PROGRESS));
        projectService.add(new Project("PROJECT_02", "Test project 2", Status.NOT_STARTED));
        projectService.add(new Project("PROJECT_26", "Test project 26", Status.IN_PROGRESS));

        taskService.add(new Task("TASK_01", "Test task 1", Status.COMPLETED));
        taskService.add(new Task("TASK_40", "Test task 40", Status.IN_PROGRESS));
        taskService.add(new Task("TASK_02", "Test task 2", Status.NOT_STARTED));
        taskService.add(new Task("TASK_26", "Test task 26", Status.IN_PROGRESS));
    }

}
