package ru.t1.kruglikov.tm.api.service;

import ru.t1.kruglikov.tm.api.repository.IProjectRepository;
import ru.t1.kruglikov.tm.enumerated.Sort;
import ru.t1.kruglikov.tm.enumerated.Status;
import ru.t1.kruglikov.tm.model.Project;

import java.util.List;

public interface IProjectService extends IProjectRepository {

    List<Project> findAll(Sort sort);

    Project create(String name, String description);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

    Project changeStatusByIndex(Integer index, Status status);

    Project changeStatusById(String id, Status status);

}
