package ru.t1.kruglikov.tm.exception.field;

public final class NumberIncorrectException extends AbstractFieldException {

    public NumberIncorrectException() {
        super("Error! Index is incorrect...");
    }

    public NumberIncorrectException(final String value, final Throwable couse) {
        super("Error! This value '" + value + "' is incorrect...");
    }

}
