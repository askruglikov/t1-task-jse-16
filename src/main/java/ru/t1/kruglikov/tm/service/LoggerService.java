package ru.t1.kruglikov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.kruglikov.tm.api.service.ILoggerService;

import java.io.IOException;
import java.util.logging.*;

public final class LoggerService implements ILoggerService {

    private static final String configFile = "/logger.properties";
    private static final String commands = "COMMANDS";
    private static final String commandsFile = "./commands.xml";
    private static final String errors = "ERRORS";
    private static final String errorsFile = "./errors.xml";
    private static final String messages = "MESSAGES";
    private static final String messagesFile = "./messages.xml";
    private static final LogManager manager = LogManager.getLogManager();
    private static final Logger loggerRoot = Logger.getLogger("");
    private static final Logger loggerCommand = Logger.getLogger(commands);
    private static final Logger loggerError = Logger.getLogger(errors);
    private static final Logger loggerMessage = Logger.getLogger(messages);
    private static final ConsoleHandler consoleHandler = getConsoleHandler();

    public Logger getLoggerCommand() {
        return loggerCommand;
    }

    public Logger getLoggerError() {
        return loggerError;
    }

    public Logger getLoggerMessage() {
        return loggerMessage;
    }

    private @NotNull
    static ConsoleHandler getConsoleHandler() {
        final ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new Formatter() {
            @Override
            public String format(LogRecord record) {
                return record.getMessage() + "\n";
            }
        });
        return handler;
    }

    public LoggerService() {
        loadConfigFromFile();
        registry(loggerCommand, commandsFile, false);
        registry(loggerError, errorsFile, true);
        registry(loggerMessage, messagesFile, true);
    }

    private void loadConfigFromFile() {
        try {
            manager.readConfiguration(LoggerService.class.getResourceAsStream(configFile));
        } catch (final IOException e) {
            loggerRoot.severe(e.getMessage());
        }
    }

    private static void registry(final Logger logger, final String fileName, final boolean isConsole) {
        try {
            if (isConsole) logger.addHandler(consoleHandler);
            logger.setUseParentHandlers(false);
            if (fileName != null && !fileName.isEmpty())
                logger.addHandler(new FileHandler(fileName));
        } catch (final IOException e) {
            loggerRoot.severe(e.getMessage());
        }
    }

    @Override
    public void info(final String message) {
        if (message == null || message.isEmpty()) return;
        loggerMessage.info(message);
    }

    @Override
    public void debug(final String message) {
        if (message == null || message.isEmpty()) return;
        loggerMessage.fine(message);
    }

    @Override
    public void command(final String message) {
        if (message == null || message.isEmpty()) return;
        loggerCommand.fine(message);
    }

    @Override
    public void error(final Exception e) {
        if (e == null) return;
        loggerError.log(Level.SEVERE, e.getMessage(), e);
    }

}
