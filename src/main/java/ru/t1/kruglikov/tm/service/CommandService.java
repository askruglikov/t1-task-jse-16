package ru.t1.kruglikov.tm.service;

import ru.t1.kruglikov.tm.api.repository.ICommandRepository;
import ru.t1.kruglikov.tm.api.service.ICommandService;
import ru.t1.kruglikov.tm.model.Command;

public final class CommandService implements ICommandService {

    private  final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository){
        this.commandRepository = commandRepository;
    }

    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
